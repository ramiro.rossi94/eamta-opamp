# Espejo de Corriente

## Como ver
1. Ejecutar el script `launch.sh`
2. Ir a **File/Load** y elegir el archivo _layout.mag_
3. Seleccionar una de las instancias PMOS (hacer click arriba y presionar `I`
4. Usar el comando `expand` para ver las subceldas.

## Estado actual
Hay un placement formado por una matriz de 5x6 subceldas.
Esta matriz fue creada de la siguiente forma:
1. Instanciado un PMOS de W = 5.4, L = 0.45, 20 fingers
2. Clonado en una matriz 5x6 con el comando `array 5 6`

Hay algunos errores de DRC para debuggear con cuidado, pero el placement esta
listo para rutear.

## Matching
La matriz esta formada por 30 bloques de 20 PMOS cada uno:
 - 1 bloque para M8
 - 7 bloques para M5
 - 21 bloques para M7
 - 1 bloque spare adicional (M1) para que la suma total sea 30

En cada bloque hay que interconectar los gates, sources and drains juntos.
Luego, la matriz se rutea empleando la siguiente distribucion de matching:

|--|--|--|--|--|
|M7|M7|M7|M7|M7|
|M7|M7|M5|M7|M7|
|M7|M5|M8|M5|M7|
|M7|M5|M5|M5|M7|
|M7|M1|M5|M7|M7|
|M7|M7|M7|M7|M7|

El ruteo necesario puede verse en el esquematico del bloque, pero se limita a:
 - Los gates de todos los bloques van juntos (IREF), salvo M1 (VDD).
 - Los sources de todos los bloques van juntos (VDD).
 - Los drains de los bloques van a sus respectivas salidas.

Todos los anillos de guarda van juntos y conectados a VDD. Se recomienda
no usar la misma conexion que para los sources para evitar que el bulk quede
a un potencial menor que VDD debido a las caidas de tension.

Usar metales lo mas anchos posible. Sobre todo para la salida de los M7, ya que
por alli circulan mas de 2 mA. Usar al menos 5 um de metal.


[[_TOC_]]

# Uso de Magic para Layout

> Video instructivo : [aqui](https://drive.google.com/file/d/1tMe_Y9q4GHimuBuMZ1ZkihKfAvpnVEgo/view)<br>
> Pagina de la herramienta : [aqui](http://opencircuitdesign.com/magic/)<br>
> Documentacion del PDK SkyWater130 : [aqui](https://skywater-pdk.readthedocs.io/en/latest/)<br>
> Pagina con tutoriales de Magic : [aqui](http://opencircuitdesign.com/magic/userguide.html)<br>

## Inicializacion
1. Abrir un terminal en la carpeta de trabajo que contiene el archivo **launchmagic.sh**
2. Ejecutar ```./launchmagic.sh```
3. Magic abre dos ventanas : una de Layout y una de comandos TCL
4. En el menu "Window" activar las opciones **Grid On** y **Snip to grid On** para que nuestro layout este en grilla
5. Guardar el espacio de trabajo con **File > Save**
6. Guardar la celda actual con **Cell > Save As**.

### Posibles problemas
1. **El script no se ejecuta** : hacerlo ejecutable con ```chmod +x launchmagic.sh```
2. **No se si Magic levanto el PDK de SKY130** : Debe decir ``` Using technology "SKY130A"``` en la terminal de comandos. Si no es asi, revisar los paths.
3. **Al abrir un layout podemos no ver nada**. Debemos ir a **File > Load** para cargar la celda.
4. **Al cargar la celda se ven cajas**. Pararse sobre una de ellas, presionar `I` para seleccionarla y usar el comando `expand` en la terminal de comandos TCL para expandir jerarquicamente.

## Etapa 1 > Placement

### Instanciacion
1. Ir a **Devices 1** o **Devices 2** y seleccionar el componente a instanciar
2. Configurar sus parametros (W, L, guardring, multiplicidad, fingering, etc...)
3. Presionar **Create** o **Create and Close** para instanciar el componente
4. Podemos enfocar la instancia creada con `Ctrl+Z`
5. Para hacer zoom in o zoom out usar `Z` o `Shift+Z`, respectivamente

### La caja de seleccion
**Magic** se maneja usando _cajas de seleccion_. Para crear una caja de seleccion:
1. Hacer **click izquierdo** en el punto que sera la **esquina inferior izquierda**.
2. Hacer **click derecho** en el punto que sera la **esquina superior derecha**.
3. Al hacer click izquierdo en otro punto se reposiciona la esquina inferior izquierda de la caja. 
4. Al hacer click derecho en otro punto se redimensiona la caja.

### Seleccion y edicion
1. Con la tecla ```I``` se selecciona la instancia debajo del puntero.
2. Con la tecla ```A``` se seleccionan todas las capas adentro de la caja de seleccion.
3. Con la tecla ```S``` se seleccionan todos los elementos adentro de la caja de seleccion.

## DRC
DRC o **Design Rules Check** es un chequeo de reglas de fabricacion que se ejecuta de forma dinamica en paralelo a la realizacion del layout. Son reglas que deben cumplirse para que el circuito sea manufacturable y este exento de posibles fallas de fisicas como latch-up o efecto antena. Una vez que el layout no posee fallas de DRC, se dice que esta **DRC Clean**.

### Dibujando capas
La mayoria de los errores de DRC se resuelven agregando capas de difusion o metal. Para ello:
1. Crear una caja de seleccion en el area a pintar
2. En la barra lateral derecha, identificar la capa con la que se desea pintar el area seleccionada.
3. Usar el boton central (ruedita) del mouse sobre la capa elegida. Esto rellena la caja de seleccion con esa capa.

### Borrado y edicion
1. Con la tecla ```D``` se borra el contenido de la caja.
2. Con la tecla ```U``` se deshace la ultima accion.
3. Con `Shift+U` se rehace la ultima accion.
4. Con la tecla `M` el contenido de la caja se mueve a la posicion actual del cursor.

### Usando la terminal de comandos
No todas las acciones tienen una combinacion de teclas asociada. Para poder usar todos los comandos disponibles, podemos usar la terminal de comandos TCL para ingresar los comandos necesarios.

En [esta pagina](http://opencircuitdesign.com/magic/userguide.html) podemos encontrar todos los comandos disponibles.

## Etapa 2 > Routing
Una vez que todos los dispositivos estan instanciados con sus parametros requeridos y no hay errores de DRC , empezamos a rutear, es decir, interconectar terminales y pines. Para eso dibujamos las capas de metal necesarias y las **vias** entre ellas.

Una via interconecta dos capas de metal consecutivas. El proceso SKY130 posee 5 capas de metal + una capa de silicio policristalino _solo reservado para conectar compuertas de transistores MOS_ (de ahi que se lo llama GP o "Gate Poly"). Las vias del proceso son:
 - VIA 1 : Conecta M1 con M2
 - VIA 2 : Conecta M2 con M3
 - etc...

Ademas tenemos **MCON**, que representa un contacto metalico entre el metal inferior y el sustrato.<br>
Esta via se usa en conexiones de gate, drain, source, y anillos de guarda.

### Creando vias
Para crear una via:
1. Armar una caja en la interseccion de metales que queremos conectar.
2. Ir a **Devices 1** y elegir el tipo de via a generar.


### Mostrando y ocultando capas
Cuando tenemos muchas capas superpuestas, podemos usar el comando `see` para mostrar u ocultar solo ciertas capas:
```bash
# Mostrar todas las capas
see

# Ocultar todas las capas
see no

# Mostrar solo metal1
see metal1

# Ocultar solo metal2
see no metal2
```

### Verificando conectividad
Para verificar todas las figuras conectadas a una en particular, colocamos el cursor sobre una figura y presionamos `S` repetidamente para ver como se "propaga" la conectividad. De esta forma podemos saber si faltan terminales por conectar o si hay elementos conectados que no deberian estarlo.

### Resistividad de cada capa de metal
Es importante tener en cuenta que los metales inferiores son bastante mas resistivos que los superiores, por lo cual deberian ser usados con anchos mayores para evitar agregar demasiada resistencia parasita que termine afectando al funcionamiento del circuito.

|Metal|Resistividad [ohms/sq]|
|-----|----------------------|
|1    |125                   |
|2    |125                   |
|3    |47                    |
|4    |47                    |
|5    |29                    |

Para las vias:

|Via  |Resistividad [ohms/sq]|
|-----|----------------------|
|1    |4500                  |
|2    |3410                  |
|3    |3410                  |
|4    |380                   |

### Routing interactivo
Magic posee una utilidad de routing interactivo que se activa de la siguiente manera:
1. En la consola, ingresar ```tool wiring```
2. Hacer click izquierdo para seleccionar el path de metal de origen
3. Al mover el cursor, vemos que se dibuja una caja que lo sigue. Si hacemos click izquierdo, empezamos a rutear.
4. Si tenemos presionado ```Shift``` y hacemos click izquierdo, la herramienta **agrega** una via y **sube** una capa de metal.
5. Si tenemos presionado ```Shift``` y hacemos click derecho, la herramienta **agrega** una via y **baja** una capa de metal.
6. Si giramos la rueda del mouse, podemos variar el width del ruteo.

Comandos asociados en este modo:
- `wire increment width`: Incrementa en 1 grilla el wire width
- `wire decrement width`: Decrementa en 1 grilla el wire width
- `wire increment type` : Incrementa en 1 nivel el metal
- `wire decrement type` : Decrementa en 1 nivel el metal

El routing interactivo es muy util para evitar colocar a mano paths y vias.<br>
Un elemento de gran ayuda para rutear es el `cross cursor`, que se activa en _Options > Crosshair_

### Manejando buses de metal
Si en vez de rutear nets individualmente optamos por manejar varias lineas de metal en simultaneo, podemos aprovechar los comandos **fill** y **corner**
1. `fill direction [metals]` estira los metales en un borde de la caja hasta el otro borde segun la direccion indicada.
2. `corner dir1 dir2 [metals]` estira los metales en una direccion y luego genera un quiebre en otra.

(Fuente : [aqui](http://opencircuitdesign.com/magic/tutorials/tut3.html))

## Pick Tool 
Es una herramienta provista por Magic de gran utilidad, que permite seleccionar una estructura de figuras y copiarla repetidas veces con un click.
1. En la terminal, colocar `tool pick`. Veremos que el curso cambia de forma.
2. Seleccionar un conjunto de figuras, colocando una caja sobre ellas y presionando **A**.
3. Pulsar el **boton medio (ruedita)** del mouse. Veremos que la seleccion se transforma en una serie de contornos que siguen al mouse.
4. Mover los contornos hasta la posicion deseada y hacer **click izquierdo**. Cada click genera una copia de las figuras seleccionadas.
5. Para terminar, presionar **click derecho**.
6. Para volver a la herramienta tradicional, ingresar el comando ```tool box```

En el paso 3, si se mantiene presionado **Shift** al presionar el boton medio, no se elimina el conjunto origen de figuras.

Podemos usar la herramienta Pick para, por ejemplo, crear un set de figuras que representan una estructura de ruteo (vias + metal) que se repite muchas veces, y luego instanciarla en todos los puntos deseados. Esto simplifica el ruteo de grandes cantidades de dispositivos, tipicamente matrices de transistores.

## LVS

Una vez que todo el layout esta terminado, solo queda debuggear el chequeo de **Layout VS Schematic** o LVS.
Este chequeo consta de dos pasos:
1. **Extraccion** de una netlist que contiene los dispositivos presentes en el layout.
2. **Comparacion** de esa netlist contra el esquematico base que se desea implementar en silicio.

### Netlists y labels

Una **Netlist** es una lista de los nodos (nets) que conforman nuestro circuito. Un ejemplo tipico de netlist es el formato [SPICE](http://bwrcs.eecs.berkeley.edu/Classes/IcBook/SPICE/), que forma la base de los numerosos simuladores como [NGSpice](http://ngspice.sourceforge.net/) o de otros softwares como [LTSpice](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html).

Para ayudar a construir o comparar netlists debemos asignar los **pines** de entrada y salida del circuito, a partir de los cuales se construye la conectividad del mismo. En Magic, esto se hace seleccionando la figura que queremos establecer como pin y asignando el label con el comando `label {NOMBRE}`. Tambien podemos usar este metodo para etiquetar nets de interes internas.

(Fuente : [aqui](http://opencircuitdesign.com/magic/tutorials/tut7.html))

### Extraccion en Magic
Se realiza con los siguientes comandos:
1. Extraer todo el layout : `extract all`
2. Transformar el archivo resultante **.ext** al formato SPICE : `ext2spice lvs; ext2spice subcircuit on; ext2spice -o {NOMBRE_LAYOUT}.spice`

(Fuente : [aqui](http://opencircuitdesign.com/magic/tutorials/tut8.html))

### Extraccion en XSchem
Necesitamos tambien la netlist del esquematico base. Para ello:
1. Abrimos el esquematico en XSchem 
2. Vamos a **Simulation/Set netlist Dir**
3. Presionamos **Netlist**

### Comparacion con Netgen
La comparacion se realiza con una herramienta externa llamada Netgen. El comando necesario es:
```bash
netgen lvs {PATH_NETLIST_SCH} {PATH_NETLIST_LAY} ~/skywater/pdk/skywater130/sky130A/libs.tech/netgen/setup.tcl
```

Donde `{PATH_NETLIST_LAY}` es el path a la netlist SPICE extraida del layout, y `{PATH_NETLIST_SCH}` es el path a la netlist generada desde el esquematico.

### Resultados de Netgen
Luego de correr Netgen, obtenemos:

1. Una ventana Tcl/Tk con un resumen de los resultados LVS finales, incluyendo cantidad de dispositivos detectados en cada netlist, cantidad de nets, etc.
2. Un archivo de salida **comp.out** con los detalles de la comparacion, incluyendo conexiones y parametros de cada instancia detectada.

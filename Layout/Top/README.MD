# TOp Level

## Como ver
1. Ejecutar el script `launch.sh`
2. Ir a **File/Load** y elegir el archivo _layout.mag_
3. Seleccionar una de las subceldas
4. Usar el comando `expand` para ver las subceldas.

## Estado actual
Se esta realizando el floorplan, colocando los tres sub bloques mediante el
comando `getcell PATH/AL/LAYOUT/DEL/SUB/BLOQUE.mag`


## Routing
A definir el pinout de cada bloque. Usar ruteo Manhattan con M1 horizontal.

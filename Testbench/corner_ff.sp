* PARAMETERS FOR CORNER FF
.param vdd = 1.98
.param iref = 105u
.options TEMP = 0.0

* Fabrication process
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib FF

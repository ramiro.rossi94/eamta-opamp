* PARAMETERS FOR CORNER SS
.param vdd = 1.62
.param iref = 95u
.options TEMP = 125.0

* Fabrication process
.lib ~/skywater/skywater-pdk/libraries/sky130_fd_pr_ngspice/latest/models/corners/sky130.lib SS

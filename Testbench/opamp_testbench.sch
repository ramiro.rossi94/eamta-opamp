v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 1132.5 -137.5 1132.5 -100 { lab=GND}
N 1132.5 -237.5 1132.5 -197.5 { lab=vss}
N 1220 -237.5 1220 -197.5 { lab=vdd}
N 1220 -137.5 1220 -100 { lab=vss}
N 1560 -240 1560 -200 { lab=vss}
N 1660 -240 1660 -200 { lab=vss}
N 1500 -430 1560 -430 { lab=#net1}
N 1500 -280 1500 -240 { lab=vss}
N 1500 -430 1500 -380 { lab=#net1}
N 1830 -400 1830 -380 { lab=vout}
N 1780 -400 1830 -400 { lab=vout}
N 1830 -320 1830 -290 { lab=vss}
N 1590 -370 1640 -370 { lab=vcm}
N 1580 -370 1590 -370 { lab=vcm}
N 1560 -430 1640 -430 { lab=#net1}
N 1640 -620 1830 -620 { lab=vout}
N 1560 -370 1560 -300 { lab=vcm}
N 1560 -370 1580 -370 { lab=vcm}
N 1710 -480 1710 -450 { lab=vdd}
N 1710 -350 1710 -320 { lab=vss}
N 1830 -410 1830 -400 { lab=vout}
N 1660 -320 1660 -300 { lab=#net2}
N 1500 -620 1580 -620 { lab=#net3}
N 1320 -340 1360 -340 { lab=#net4}
N 1470 -340 1500 -340 { lab=#net1}
N 1500 -380 1500 -340 { lab=#net1}
N 1390 -430 1430 -430 { lab=#net3}
N 1490 -430 1500 -430 { lab=#net1}
N 1410 -620 1410 -430 { lab=#net3}
N 1410 -620 1500 -620 { lab=#net3}
N 1440 -340 1470 -340 { lab=#net1}
N 1360 -340 1380 -340 { lab=#net4}
N 1320 -430 1330 -430 { lab=#net5}
N 1320 -430 1320 -410 { lab=#net5}
N 1290 -430 1320 -430 { lab=#net5}
N 1320 -350 1320 -340 { lab=#net4}
N 1210 -430 1230 -430 { lab=vcm}
N 1320 -280 1320 -200 { lab=vsen}
N 1320 -140 1320 -100 { lab=vcm}
N 1830 -620 1830 -550 { lab=vout}
N 1830 -490 1830 -410 { lab=vout}
N 1830 -550 1830 -490 { lab=vout}
C {vsource.sym} 1132.5 -167.5 0 0 {name=V1 value=DC\{vss\}}
C {vsource.sym} 1220 -167.5 0 0 {name=V2 value=DC\{vdd\}}
C {vsource.sym} 1560 -270 0 0 {name=V3 value=DC\{vcm\}}
C {gnd.sym} 1132.5 -100 0 0 {name=l14 lab=GND}
C {vsource.sym} 1320 -170 0 0 {name=V4 value="sin(0 \{vac\} 1Meg) dc 0 ac 1"}
C {capa.sym} 1320 -310 2 0 {name=C4
m=1
value=1
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} 1220 -237.5 1 0 {name=l15 sig_type=std_logic lab=vdd}
C {lab_pin.sym} 1132.5 -237.5 1 0 {name=l16 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1220 -100 3 0 {name=l18 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1560 -200 3 0 {name=l19 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1320 -100 3 0 {name=l20 sig_type=std_logic lab=vcm}
C {isource.sym} 1660 -270 0 0 {name=I0 value=DC\{iref\}}
C {lab_pin.sym} 1660 -200 3 0 {name=l22 sig_type=std_logic lab=vss}
C {lab_wire.sym} 1320 -255 3 0 {name=l24 sig_type=std_logic lab=vsen}
C {res.sym} 1360 -430 1 0 {name=R1
value=500
footprint=1206
device=resistor
m=1}
C {res.sym} 1610 -620 1 0 {name=R3
value=5k
footprint=1206
device=resistor
m=1}
C {lab_pin.sym} 1500 -240 3 0 {name=l28 sig_type=std_logic lab=vss
}
C {capa.sym} 1500 -310 0 0 {name=C5
m=1
value=5p
footprint=1206
device="ceramic capacitor"}
C {netlist_not_shown.sym} 1080 -600 0 0 {name=SIMULATION only_toplevel=false 

value="

* Select corner
.include /home/eamta/eamta2021/sch/OPAMP/corner_ss.sp

* Input Parameters
.param vss = 0.0
.param vcm = vdd/2
.param vac = 70m

* Design parameters

* Current reference
.param wpref = 5.4
.param lpref = 0.45
.param mrefout = 420
.param mrefdif = 140
.param mpref = 20

* Differential pair 0p6
.param mpdif = 40
.param wpdif = 7.35
.param lpdif = 1.2

* Current mirror load
.param wndio = 4.2
.param lndio = 0.45
.param mndio = 4

* Output stage
.param mnout = 240
.param wnout = 1.95
.param lnout = 0.15

* Miller compensation
.param wncom = 1.5
.param lncom = 1.8
.param mncom = 100
.param wcap = 45
.param lcap = 45

* 2 fF/um2

* 4 um -> 0.75 pF

* Sim select
.param rol = 0
.param rcl = 100G

* Include Models

* OP Parameters & Signals to save
.save all
+ @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[id] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[vth] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[vgs] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[vds] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[vdsat] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[gm] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[gds]
+ @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[id] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[vth] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[vgs] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[vds] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[vdsat] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[gm] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[gds]
+ @M.X1.XM3.msky130_fd_pr__nfet_01v8[id] @M.X1.XM3.msky130_fd_pr__nfet_01v8[vth] @M.X1.XM3.msky130_fd_pr__nfet_01v8[vgs] @M.X1.XM3.msky130_fd_pr__nfet_01v8[vds] @M.X1.XM3.msky130_fd_pr__nfet_01v8[vdsat] @M.X1.XM3.msky130_fd_pr__nfet_01v8[gm] @M.X1.XM3.msky130_fd_pr__nfet_01v8[gds]
+ @M.X1.XM4.msky130_fd_pr__nfet_01v8[id] @M.X1.XM4.msky130_fd_pr__nfet_01v8[vth] @M.X1.XM4.msky130_fd_pr__nfet_01v8[vgs] @M.X1.XM4.msky130_fd_pr__nfet_01v8[vds] @M.X1.XM4.msky130_fd_pr__nfet_01v8[vdsat] @M.X1.XM4.msky130_fd_pr__nfet_01v8[gm] @M.X1.XM4.msky130_fd_pr__nfet_01v8[gds]
+ @M.X1.XM5.msky130_fd_pr__pfet_01v8[id] @M.X1.XM5.msky130_fd_pr__pfet_01v8[vth] @M.X1.XM5.msky130_fd_pr__pfet_01v8[vgs] @M.X1.XM5.msky130_fd_pr__pfet_01v8[vds] @M.X1.XM5.msky130_fd_pr__pfet_01v8[vdsat] @M.X1.XM5.msky130_fd_pr__pfet_01v8[gm] @M.X1.XM5.msky130_fd_pr__pfet_01v8[gds]
+ @M.X1.XM6.msky130_fd_pr__nfet_01v8[id] @M.X1.XM6.msky130_fd_pr__nfet_01v8[vth] @M.X1.XM6.msky130_fd_pr__nfet_01v8[vgs] @M.X1.XM6.msky130_fd_pr__nfet_01v8[vds] @M.X1.XM6.msky130_fd_pr__nfet_01v8[vdsat] @M.X1.XM6.msky130_fd_pr__nfet_01v8[gm] @M.X1.XM6.msky130_fd_pr__nfet_01v8[gds]
+ @M.X1.XM7.msky130_fd_pr__pfet_01v8[id] @M.X1.XM7.msky130_fd_pr__pfet_01v8[vth] @M.X1.XM7.msky130_fd_pr__pfet_01v8[vgs] @M.X1.XM7.msky130_fd_pr__pfet_01v8[vds] @M.X1.XM7.msky130_fd_pr__pfet_01v8[vdsat] @M.X1.XM7.msky130_fd_pr__pfet_01v8[gm] @M.X1.XM7.msky130_fd_pr__pfet_01v8[gds]
+ @M.X1.XM8.msky130_fd_pr__pfet_01v8[id] @M.X1.XM8.msky130_fd_pr__pfet_01v8[vth] @M.X1.XM8.msky130_fd_pr__pfet_01v8[vgs] @M.X1.XM8.msky130_fd_pr__pfet_01v8[vds] @M.X1.XM8.msky130_fd_pr__pfet_01v8[vdsat] @M.X1.XM8.msky130_fd_pr__pfet_01v8[gm] @M.X1.XM8.msky130_fd_pr__pfet_01v8[gds]
+ @M.X1.XM9.msky130_fd_pr__nfet_01v8[id] @M.X1.XM9.msky130_fd_pr__nfet_01v8[vth] @M.X1.XM9.msky130_fd_pr__nfet_01v8[vgs] @M.X1.XM9.msky130_fd_pr__nfet_01v8[vds] @M.X1.XM9.msky130_fd_pr__nfet_01v8[vdsat] @M.X1.XM9.msky130_fd_pr__nfet_01v8[gm] @M.X1.XM9.msky130_fd_pr__nfet_01v8[gds]
+ @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[cgs] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[cgd] @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[cds]
+ @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[cgs] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[cgd] @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[cds]
+ @M.X1.XM3.msky130_fd_pr__nfet_01v8[cgs] @M.X1.XM3.msky130_fd_pr__nfet_01v8[cgd] @M.X1.XM3.msky130_fd_pr__nfet_01v8[cds]
+ @M.X1.XM4.msky130_fd_pr__nfet_01v8[cgs] @M.X1.XM4.msky130_fd_pr__nfet_01v8[cgd] @M.X1.XM4.msky130_fd_pr__nfet_01v8[cds]
+ @M.X1.XM5.msky130_fd_pr__pfet_01v8[cgs] @M.X1.XM5.msky130_fd_pr__pfet_01v8[cgd] @M.X1.XM5.msky130_fd_pr__pfet_01v8[cds]
+ @M.X1.XM6.msky130_fd_pr__nfet_01v8[cgs] @M.X1.XM6.msky130_fd_pr__nfet_01v8[cgd] @M.X1.XM6.msky130_fd_pr__pfet_01v8[cds]
+ @M.X1.XM7.msky130_fd_pr__pfet_01v8[cgs] @M.X1.XM7.msky130_fd_pr__pfet_01v8[cgd] @M.X1.XM7.msky130_fd_pr__nfet_01v8[cds]
+ @M.X1.XM8.msky130_fd_pr__pfet_01v8[cgs] @M.X1.XM8.msky130_fd_pr__pfet_01v8[cgd] @M.X1.XM8.msky130_fd_pr__pfet_01v8[cds]
+ @M.X1.XM9.msky130_fd_pr__nfet_01v8[cgs] @M.X1.XM9.msky130_fd_pr__nfet_01v8[cgd] @M.X1.XM9.msky130_fd_pr__pfet_01v8[cds]

*Simulation
.control

   * Operation Point
   op
   save all
   write opamp_testbench.raw

   * Small-signal parasitics
   echo '------------------------ OPERATION POINT ANALYSIS ------------------------'
   print @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[cgs]
   print @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[cgs]
   print @M.X1.XM3.msky130_fd_pr__nfet_01v8[cgs]
   print @M.X1.XM4.msky130_fd_pr__nfet_01v8[cgs]
   print @M.X1.XM5.msky130_fd_pr__pfet_01v8[cgs]
   print @M.X1.XM6.msky130_fd_pr__nfet_01v8[cgs]
   print @M.X1.XM7.msky130_fd_pr__pfet_01v8[cgs]
   print @M.X1.XM8.msky130_fd_pr__pfet_01v8[cgs]

   print @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[cgd]
   print @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[cgd]
   print @M.X1.XM3.msky130_fd_pr__nfet_01v8[cgd]
   print @M.X1.XM4.msky130_fd_pr__nfet_01v8[cgd]
   print @M.X1.XM5.msky130_fd_pr__pfet_01v8[cgd]
   print @M.X1.XM6.msky130_fd_pr__nfet_01v8[cgd]
   print @M.X1.XM7.msky130_fd_pr__pfet_01v8[cgd]
   print @M.X1.XM8.msky130_fd_pr__pfet_01v8[cgd]

   print @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[cds]
   print @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[cds]
   print @M.X1.XM3.msky130_fd_pr__nfet_01v8[cds]
   print @M.X1.XM4.msky130_fd_pr__nfet_01v8[cds]
   print @M.X1.XM5.msky130_fd_pr__pfet_01v8[cds]
   print @M.X1.XM6.msky130_fd_pr__nfet_01v8[cds]
   print @M.X1.XM7.msky130_fd_pr__pfet_01v8[cds]
   print @M.X1.XM8.msky130_fd_pr__pfet_01v8[cds]

   print @M.X1.XM1.msky130_fd_pr__pfet_01v8_lvt[gds]
   print @M.X1.XM2.msky130_fd_pr__pfet_01v8_lvt[gds]
   print @M.X1.XM3.msky130_fd_pr__nfet_01v8[gds]
   print @M.X1.XM4.msky130_fd_pr__nfet_01v8[gds]
   print @M.X1.XM5.msky130_fd_pr__pfet_01v8[gds]
   print @M.X1.XM6.msky130_fd_pr__nfet_01v8[gds]
   print @M.X1.XM7.msky130_fd_pr__pfet_01v8[gds]
   print @M.X1.XM8.msky130_fd_pr__pfet_01v8[gds]

   * AC Analysis - Open Loop
   echo '------------------------ OPEN LOOP ANALYSIS ------------------------'
   ac dec 100 1k 1G
   meas ac GBW when vdb(vout)=0
   meas ac DCG find vdb(vout) at=2k
   meas ac PM find vp(vout) when vdb(vout)=0
   print PM*180/PI
   meas ac GM find vdb(vout) when vp(vout)=0
  
   plot vdb(vout) \{vp(vout)*180/PI\} title 'Open Loop'

   set filetype=ascii
   write AC_Analysis_open.raw

   * AC Analysis - Closed Loop
   echo '------------------------ CLOSED LOOP ANALYSIS ------------------------'
   alterparam rol = 100G
   alterparam rcl = 0
   reset
   ac dec 100 1k 1G
   save all
   meas ac GBW when vdb(vout)=0
   meas ac DCG find vdb(vout) at=1k
  
   plot vdb(vout) \{vp(vout)*180/PI\} title 'Closed Loop'

   set filetype=ascii
   write AC_Analysis_closed.raw

   * Noise Analysis
   echo '------------------------ NOISE ANALYSIS ------------------------'
   noise v(vout) V4 dec 100 1k 10G 1
   setplot noise1
   print inoise_total
   print onoise_total

   print onoise_total.m.x1.xm1.msky130_fd_pr__pfet_01v8_lvt.1overf
   print onoise_total.m.x1.xm1.msky130_fd_pr__pfet_01v8_lvt.id
   print onoise_total.m.x1.xm2.msky130_fd_pr__pfet_01v8_lvt.1overf
   print onoise_total.m.x1.xm2.msky130_fd_pr__pfet_01v8_lvt.id
   print onoise_total.m.x1.xm3.msky130_fd_pr__nfet_01v8.1overf
   print onoise_total.m.x1.xm3.msky130_fd_pr__nfet_01v8.id
   print onoise_total.m.x1.xm4.msky130_fd_pr__nfet_01v8.1overf
   print onoise_total.m.x1.xm4.msky130_fd_pr__nfet_01v8.id
   print onoise_total.m.x1.xm5.msky130_fd_pr__pfet_01v8.1overf
   print onoise_total.m.x1.xm5.msky130_fd_pr__pfet_01v8.id
   print onoise_total.m.x1.xm6.msky130_fd_pr__nfet_01v8.1overf
   print onoise_total.m.x1.xm6.msky130_fd_pr__nfet_01v8.id
   print onoise_total.m.x1.xm7.msky130_fd_pr__pfet_01v8.1overf
   print onoise_total.m.x1.xm7.msky130_fd_pr__pfet_01v8.id
   print onoise_total.m.x1.xm8.msky130_fd_pr__pfet_01v8.1overf
   print onoise_total.m.x1.xm8.msky130_fd_pr__pfet_01v8.id
   print onoise_total.m.x1.xm9.msky130_fd_pr__nfet_01v8.1overf
   print onoise_total.m.x1.xm9.msky130_fd_pr__nfet_01v8.id
   print onoise_total_r.x1.xc1.rs1_1overf	
   print onoise_total_r.x1.xc1.rs1_thermal
   print onoise_total_r.x1.xc1.rs2_1overf
   print onoise_total_r.x1.xc1.rs1_thermal
   print onoise_total_r1_1overf
   print onoise_total_r1_thermal
   print onoise_total_r3_1overf
   print onoise_total_r3_thermal
   reset 

   echo '------------------------ THD ANALYSIS ------------------------'   
   tran 0.01u 11u
   plot v(vout) title 'Sine Output'
   fourier 1e6 vout
   let THD_db = db(fourier11[1][3]/fourier11[1][1])
   print THD_db
   fft vout
   plot mag(vout)
   plot db(mag(vout))

   * Parametric sweep
   echo '=================== PARAMETRIC SWEEPS ====================='
   reset
   foreach ll
     
      alterparam lnout = $ll
      reset
      print $ll

      * Open loop
      echo '------------------------ Open Loop response ------------------------'
      alterparam rol = 0
      alterparam rcl = 100G
      reset
      ac dec 100 1k 1G
      save all
      meas ac GBW when vdb(vout)=0
      meas ac DCG find vdb(vout) at=2k
      meas ac PM find vp(vout) when vdb(vout)=0
      print PM*180/PI

      * Closed loop
      echo '----------------------- Closed Loop response ------------------------'
      alterparam rol = 100G
      alterparam rcl = 0
      reset
      ac dec 100 1k 1G
      save all
      ac dec 100 1k 1G
      meas ac GBW when vdb(vout)=0
      meas ac DCG find vdb(vout) at=2k

   end

.endc

.end
"}
C {capa.sym} 1830 -350 0 0 {name=C1
m=1
value=20p
footprint=1206
device="ceramic capacitor"}
C {lab_pin.sym} 1830 -290 3 0 {name=l2 sig_type=std_logic lab=vss}
C {lab_wire.sym} 1822.5 -400 0 0 {name=l3 sig_type=std_logic lab=vout

}
C {lab_wire.sym} 1622.5 -370 0 0 {name=l5 sig_type=std_logic lab=vcm

}
C {ngspice_probe.sym} 1830 -400 0 0 {name=r1}
C {lab_pin.sym} 1710 -320 3 0 {name=l1 sig_type=std_logic lab=vss}
C {lab_pin.sym} 1710 -477.5 1 0 {name=l4 sig_type=std_logic lab=vdd}
C {ngspice_probe.sym} 1560 -320 0 0 {name=r2}
C {res.sym} 1410 -340 3 0 {name=R4
value=\{rol\}
footprint=1206
device=resistor
m=1}
C {res.sym} 1460 -430 1 1 {name=R2
value=\{rcl\}
footprint=1206
device=resistor
m=1}
C {res.sym} 1320 -380 0 0 {name=R5
value=\{rcl\}
footprint=1206
device=resistor
m=1}
C {res.sym} 1260 -430 1 0 {name=R6
value=\{rol\}
footprint=1206
device=resistor
m=1}
C {lab_wire.sym} 1222.5 -430 0 0 {name=l6 sig_type=std_logic lab=vcm

}
C {OPAMP/opamp.sym} 1700 -400 0 0 {name=x1}

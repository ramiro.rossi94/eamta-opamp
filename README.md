# EAMTA OPAMP

Two stages Miller OPAMP designed for analog desging course of EAMTA

Instrucciones:
- Entrar a la VM con la pass 3amt4
- Lanzar xschem en la carpeta sch donde se ubica el archivo de configuracion xschemrc
- Abrir el archivo de top level opamp_testbench.sch en la misma carpeta en la que se encuentra xschemrc
- Para descender dentro del OPAMP, seleccionar el simbolo y pulsar E
- Para regresar a top level, usar Ctrl+E
- Todos los dispositivos del OPAMP estan parametrizados, y solo deberian modificarse a traves del archivo SPICE llamado SIMULATION e incrustado en el testbench.
- Para correr el testbench, presionar Netlist y luego SImulate. Si sale bien, lanza una consola con el reporte de resultados y los graficos que se programaron en SPICE.

- El testbench reporta:
  a) Capacidades parasitas y conductancias drain-source de cada MOS
  b) A lazo abierto : Bode, ganancia, producto ganancia - BW y margen de fase
  c) A lazo cerrado : Bode, ganancia, producto ganancia - BW, ruido
  d) Tambien a lazo cerrado se muestra una onda de salida de 1,2 Vpp y la distorsi[on que sufre, a traves del THD y del espectro de Fourier.

Despues de correr el testbench, descender al OPAMP y hacer Ctrl + Click en la flecha que dice Annotate. Esto va a anotar las tensiones, corrientes y transconductancias de todos los elementos en el esquematico para ver su polarizacion.

Para cambiar el corner que se simula, solo modificar la linea del SPICE que dice .include para incluir los archivos corner_tt, corner_ff o corner_ss seg[un se requiera.
